package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.out.persistence.textAnalysis;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "text_analysis")
public class TextAnalysisJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "text_analysis_id")
    private Long text_analysis_id;

    @Column(name = "transcription_id")
    private Long transcription_id;

    @Column(name = "text_analysis_url")
    private String text_analysis_url;

    @Column(name = "job_state")
    private Integer job_state;
}
