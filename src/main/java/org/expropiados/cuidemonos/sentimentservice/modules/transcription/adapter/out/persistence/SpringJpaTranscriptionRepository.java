package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.out.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface SpringJpaTranscriptionRepository extends JpaRepository<TranscriptionEntity, Long> {
    @Query(value = "SELECT * " +
            "FROM sentiment.transcription T " +
            "WHERE T.videocall_uuid=?1", nativeQuery = true)
    TranscriptionEntity findByVideocallUuid(UUID videocallUuid);

    @Query(value = "SELECT * " +
            "FROM sentiment.transcription T " +
            "WHERE T.appointment_id=?1 " +
            "ORDER BY T.TRANSCRIPTION_ID DESC " +
            "LIMIT 1", nativeQuery = true)
    TranscriptionEntity findByAppointmentId(Long appointmentId);
}
