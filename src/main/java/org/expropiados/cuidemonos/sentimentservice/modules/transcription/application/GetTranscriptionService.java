package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.GetTranscriptionUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TranscriptionPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;

import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class GetTranscriptionService implements GetTranscriptionUseCase {
    private final TranscriptionPort transcriptionPort;

    @Override
    public Transcription getTranscription(Long appointmentId){
        return transcriptionPort.getTranscription(appointmentId);
    }
}
