package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;

import java.util.UUID;

public interface GetAnalysisUseCase {
    TextAnalysis getTextAnalysis(Long transcription_id);
}
