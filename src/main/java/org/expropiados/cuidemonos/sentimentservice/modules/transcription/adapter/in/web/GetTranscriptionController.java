package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.in.web;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.GetTranscriptionUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.SaveTranscriptionUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.UUID;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
public class GetTranscriptionController {

    private final GetTranscriptionUseCase getTranscriptionUseCase;

    @GetMapping("/transcriptions/{appointmentId}")
    @ApiOperation(value="Gets a transcription")
    public ResponseEntity<Transcription> getTranscription(@PathVariable Long appointmentId){
        var transcription = getTranscriptionUseCase.getTranscription(appointmentId);
        if(transcription == null) return new ResponseEntity<>((Transcription) null,HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(transcription, HttpStatus.OK);
    }
}
