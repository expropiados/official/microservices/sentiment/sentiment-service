package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;

public interface TextAnalysisPort {
    TextAnalysis save(Long transcriptionId);
    TextAnalysis update(Long transcriptionId, String textAnalysisUrl, Integer jobState);
    TextAnalysis get(Long transcription_id);
}
