package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;

import java.time.LocalDateTime;
import java.util.UUID;

public interface SaveTranscriptionUseCase {
    Transcription saveTranscription(Long transcription_id,
                                    String transcription_url, Integer job_state);
    void initializeTranscription(Long transcriptionId, String transcription_url);
}
