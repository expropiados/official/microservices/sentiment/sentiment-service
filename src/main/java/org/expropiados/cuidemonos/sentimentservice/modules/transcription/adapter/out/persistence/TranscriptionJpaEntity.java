package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.out.persistence;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Table(name = "transcription")
public class TranscriptionJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transcription_id")
    private Long transcription_id;

    @Column(name = "videocall_uuid")
    private UUID videocall_uuid;

    @Column(name = "appointment_id")
    private Long appointmentId;

    @Column(name = "user_id", length = 100)
    private String userId;

    @Column(name = "transcription_url")
    private String transcription_url;

    @Column(name = "job_state")
    private Integer job_state;

    @Column(name = "request_datetime")
    private LocalDateTime request_datetime;

    @Column(name = "termination_datetime")
    private LocalDateTime termination_datetime;

    @Column(name = "audio_url")
    private String audio_url;
}
