package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.out.persistence;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TranscriptionPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;

@PersistenceAdapter
@RequiredArgsConstructor
public class TranscriptionPersistenceAdapter implements TranscriptionPort {

    private final SpringJpaTranscriptionRepository transcriptionRepository;
    private final TranscriptionMapper transcriptionMapper;

    @Override
    public Long save(Transcription transcription) {
        var entityJpa = transcriptionMapper.toTranscriptionEntity(transcription);
        var saved = transcriptionRepository.save(entityJpa);
        var transcriptionSaved = transcriptionMapper.toTranscription(saved);
        return transcriptionSaved.getId();
    }

    @Override
    public Transcription getTranscription(Long appointmentId) {
        var transcriptionEntity = transcriptionRepository.findByAppointmentId(appointmentId);
        return transcriptionMapper.toTranscription(transcriptionEntity);
    }

    @Override
    public Transcription saveTranscription(Transcription transcription){
        var TEnc = transcriptionRepository.findById(transcription.getId());
        if (TEnc.isPresent()){ //Si esta presente, actualiza
            var TEncE = TEnc.get();
            TEncE.setTranscriptionId(transcription.getId());
            TEncE.setTranscriptionUrl(transcription.getTranscriptionUrl());
            TEncE.setJobState(transcription.getJobState());
            var saved = transcriptionRepository.save(TEncE);
            var transcriptionSaved = transcriptionMapper.toTranscription(saved);
            return transcriptionSaved;
        }
        // Si no esta presente, registra nuevo
        var entityJpa = transcriptionMapper.toTranscriptionEntity(transcription);
        var saved = transcriptionRepository.save(entityJpa);
        var transcriptionSaved = transcriptionMapper.toTranscription(saved);
        return transcriptionSaved;
    }
}
