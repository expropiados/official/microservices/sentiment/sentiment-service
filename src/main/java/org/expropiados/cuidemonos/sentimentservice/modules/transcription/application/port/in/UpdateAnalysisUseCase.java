package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;

public interface UpdateAnalysisUseCase {
    TextAnalysis updateAnalysis(Long textAnalysisId, String textAnalysisUrl, Integer jobState);
}
