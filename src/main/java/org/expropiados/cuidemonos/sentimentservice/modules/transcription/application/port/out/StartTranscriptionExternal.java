package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StartTranscriptionExternal {
    Long transcription_id;
    String transcription_url;
}
