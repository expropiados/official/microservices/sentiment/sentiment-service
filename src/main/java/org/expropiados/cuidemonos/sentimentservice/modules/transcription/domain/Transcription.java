package org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Transcription {
    private Long id;
    private UUID videocallUuid;
    private String transcriptionUrl;
    private String audioUrl;
    private Integer jobState;
    private String userId;
    private LocalDateTime requestDatetime;
    private LocalDateTime terminationDatetime;
    private Long appointmentId;
}
