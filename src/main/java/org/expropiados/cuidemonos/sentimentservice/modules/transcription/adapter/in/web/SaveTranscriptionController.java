package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.in.web;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.SaveTranscriptionUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.UUID;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
public class SaveTranscriptionController {
    private final SaveTranscriptionUseCase saveTranscriptionUseCase;

    @PostMapping("/saveTranscription")
    @ApiOperation(value="Saves a transcription")
    public Transcription saveTranscription(@RequestParam(value="transcription_id") Long transcription_id,
                                           @RequestParam(value="transcription_url") String transcription_url,
                                           @RequestParam(value="job_state") Integer job_state){
        return saveTranscriptionUseCase.saveTranscription(transcription_id,
                transcription_url, job_state);
    }
}
