package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.out.persistence.textAnalysis;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface TextAnalysisMapper {
    @Mapping(source = "text_analysis_id", target = "id")
    @Mapping(source = "transcription_id", target = "transcriptionId")
    @Mapping(source = "text_analysis_url", target = "textAnalysisUrl")
    @Mapping(source = "job_state", target = "jobState")
    TextAnalysis toTextAnalysis(TextAnalysisJpaEntity textAnalysisJpaEntity);

    @InheritInverseConfiguration
    TextAnalysisJpaEntity toTextAnalysisEntity(TextAnalysis textAnalysis);
}
