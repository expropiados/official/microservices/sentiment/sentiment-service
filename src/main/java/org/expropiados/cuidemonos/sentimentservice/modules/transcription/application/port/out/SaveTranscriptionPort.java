package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.RegisterTranscriptionCommand;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

public interface SaveTranscriptionPort {
    /*Optional<Transcription> saveTranscription(RegisterTranscriptionCommand command,UUID transcriptionUUID);*/
    Transcription saveTranscription(Transcription transcription);
}
