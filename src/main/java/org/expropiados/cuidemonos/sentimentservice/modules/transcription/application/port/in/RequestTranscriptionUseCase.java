package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in;


import java.util.UUID;

public interface RequestTranscriptionUseCase {
    Long requestTranscription(String videocallDir, UUID videocallUuid, String userId, Long appointmentId);
    void initializeTranscription(Long transcriptionId, String videocallDir, String userId);
}
