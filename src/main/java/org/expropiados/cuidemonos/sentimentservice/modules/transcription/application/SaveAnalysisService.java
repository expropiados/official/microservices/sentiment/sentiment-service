package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.SaveAnalysisUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TextAnalysisPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;

@UseCase
@RequiredArgsConstructor
public class SaveAnalysisService implements SaveAnalysisUseCase {

    private final TextAnalysisPort textAnalysisPort;

    @Override
    public TextAnalysis saveAnalysis(Long transcriptionId) {
        return textAnalysisPort.save(transcriptionId);
    }
}
