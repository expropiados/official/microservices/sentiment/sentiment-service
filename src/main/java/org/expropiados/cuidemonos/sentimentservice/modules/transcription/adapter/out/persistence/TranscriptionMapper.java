package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.out.persistence;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel="spring")
public interface TranscriptionMapper {

    @Mapping(source = "transcriptionId", target = "id")
    @Mapping(source = "videocallUuid", target = "videocallUuid")
    @Mapping(source = "appointmentId", target = "appointmentId")
    @Mapping(source = "userId", target = "userId")
    @Mapping(source = "transcriptionUrl", target = "transcriptionUrl")
    @Mapping(source = "audioUrl", target = "audioUrl")
    @Mapping(source = "jobState", target = "jobState")
    @Mapping(source = "requestDatetime", target = "requestDatetime")
    @Mapping(source = "terminationDatetime", target = "terminationDatetime")
    Transcription toTranscription(TranscriptionEntity transcriptionEntity);

    @InheritInverseConfiguration
    TranscriptionEntity toTranscriptionEntity(Transcription transcription);
}
