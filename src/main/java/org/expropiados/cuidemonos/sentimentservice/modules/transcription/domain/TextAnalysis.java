package org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TextAnalysis {
    private Long id;
    private Long transcriptionId;
    private String textAnalysisUrl;
    private Integer jobState;
}
