package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.in.web;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.RequestTranscriptionUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.RequestedTranscription;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
public class RequestTranscriptionController {
    private final RequestTranscriptionUseCase requestTranscriptionUseCase;

    @PostMapping("/transcribe")
    @ApiOperation(value="Request the transcription of a videocall")
    public Long requestVideocall(@RequestBody RequestedTranscription requestedTranscription) {

        Long transcriptionId = requestTranscriptionUseCase.requestTranscription(
                requestedTranscription.getVideocallDir(),
                requestedTranscription.getVideocallUuid(),
                requestedTranscription.getUserId(),
                requestedTranscription.getAppointmentId()
        );

        requestTranscriptionUseCase.initializeTranscription(
                transcriptionId,
                requestedTranscription.getVideocallDir(),
                requestedTranscription.getUserId()
        );

        return transcriptionId;
    }
}
