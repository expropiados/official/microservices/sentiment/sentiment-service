package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestTranscriptionExternal {
    Long transcription_id;
    String videocall_dir;
    String user_id;
}
