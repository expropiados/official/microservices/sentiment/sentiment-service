package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application;

import com.google.common.net.HttpHeaders;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.RequestTranscriptionUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.RequestTranscriptionExternal;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TextAnalysisPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TranscriptionPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.UUID;

@UseCase
@RequiredArgsConstructor
public class RequestTranscriptionService implements RequestTranscriptionUseCase {

    private static final Logger logger = LoggerFactory.getLogger(RequestTranscriptionService.class);
    private final TranscriptionPort transcriptionPort;
    private final TextAnalysisPort textAnalysisPort;

    @Value("${sentiment.job.transcribe.url}")
    private String externalTranscribeService;

    @Override
    public Long requestTranscription(String videocallDir, UUID videocallUuid, String userId, Long appointmentId) {
        var transcription = new Transcription();
        transcription.setAudioUrl(videocallDir);
        transcription.setVideocallUuid(videocallUuid);
        transcription.setUserId(userId);
        transcription.setAppointmentId(appointmentId);
        transcription.setRequestDatetime(LocalDateTime.now(ZoneId.of("America/Lima")));
        transcription.setJobState(0);

        var transcriptionId = transcriptionPort.save(transcription);
        textAnalysisPort.save(transcriptionId);

        return transcriptionId;
    }

    @Override
    public void initializeTranscription(Long transcriptionId, String videoCallDir, String userId) {
        var params = new RequestTranscriptionExternal(transcriptionId, videoCallDir, userId);
        var webClient = WebClient.create(externalTranscribeService);

        logger.info("New request to the endpoint: {}", externalTranscribeService);

        var response = webClient.post()
                .uri("/jobtranscribe")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .body(Mono.just(params), RequestTranscriptionExternal.class)
                .retrieve()
                .bodyToMono(String.class)
                .block();

        logger.info("Response of the request: {}", response);
    }
}
