package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application;

import com.google.common.net.HttpHeaders;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.SaveTranscriptionUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.StartTranscriptionExternal;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TranscriptionPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

@UseCase
@RequiredArgsConstructor
public class SaveTranscriptionService implements SaveTranscriptionUseCase {

    private final TranscriptionPort transcriptionPort;
    private static final Logger logger = LoggerFactory.getLogger(RequestTranscriptionService.class);

    @Value("${sentiment.job.transcribe.url}")
    private String externalTranscribeService;

    @Override
    public void initializeTranscription(Long transcriptionId, String transcription_url) {
        var params = new StartTranscriptionExternal(transcriptionId, transcription_url);
        var webClient = WebClient.create(externalTranscribeService);

        logger.info("New request to the endpoint: {}", externalTranscribeService);

        var response = webClient.post()
                .uri(builder -> builder.path("/jobanalysis")
                        .queryParam("transcription_id", transcriptionId.toString())
                        .queryParam("transcription_url", transcription_url)
                        .build())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .retrieve()
                .bodyToMono(String.class)
                .block();

        logger.info("Response of the request: {}", response);
    }

    @Override
    public Transcription saveTranscription(Long transcription_id,
                                           String transcription_url, Integer job_state){

        this.initializeTranscription(transcription_id, transcription_url);

        Transcription Ts = Transcription.builder().
                id(transcription_id).
                transcriptionUrl(transcription_url).jobState(job_state).build();
        /*
        Transcription Ts = new Transcription(transcription_id, UUID.randomUUID(),
                transcription_url, job_state,
                LocalDateTime.now(), LocalDateTime.now(),
                "AUDIO URL");
        */
        return transcriptionPort.saveTranscription(Ts);
    }
}
