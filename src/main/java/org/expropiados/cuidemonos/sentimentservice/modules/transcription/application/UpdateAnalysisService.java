package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.UpdateAnalysisUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TextAnalysisPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;

@UseCase
@RequiredArgsConstructor
public class UpdateAnalysisService implements UpdateAnalysisUseCase {
    private final TextAnalysisPort textAnalysisPort;

    @Override
    public TextAnalysis updateAnalysis(Long textAnalysisId, String textAnalysisUrl, Integer jobState) {
        return textAnalysisPort.update(textAnalysisId, textAnalysisUrl, jobState);
    }
}
