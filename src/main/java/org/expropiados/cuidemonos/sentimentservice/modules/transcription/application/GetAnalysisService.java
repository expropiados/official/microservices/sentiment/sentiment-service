package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.UseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.GetAnalysisUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TextAnalysisPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;

@UseCase
@RequiredArgsConstructor
public class GetAnalysisService implements GetAnalysisUseCase {
    private final TextAnalysisPort textAnalysisPort;

    @Override
    public TextAnalysis getTextAnalysis(Long id){
        return textAnalysisPort.get(id);
    }
}
