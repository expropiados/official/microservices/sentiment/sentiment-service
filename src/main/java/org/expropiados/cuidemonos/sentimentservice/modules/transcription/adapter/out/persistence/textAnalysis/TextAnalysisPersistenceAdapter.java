package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.out.persistence.textAnalysis;

import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.PersistenceAdapter;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.out.persistence.SpringJpaTranscriptionRepository;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TextAnalysisPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;

@PersistenceAdapter
@RequiredArgsConstructor
public class TextAnalysisPersistenceAdapter implements TextAnalysisPort {
    private final SpringJpaTextAnalysisRepository textAnalysisRepository;
    //private final TextAnalysisRepository textAnalysisRepositoryNoJpa;
    private final SpringJpaTranscriptionRepository transcriptionRepository;
    private final TextAnalysisMapper textAnalysisMapper;

    @Override
    public TextAnalysis save(Long transcriptionId) {
        var toBeSaved = new TextAnalysis();
        toBeSaved.setTranscriptionId(transcriptionId);
        toBeSaved.setJobState(0);

        var entityJpa = textAnalysisMapper.toTextAnalysisEntity(toBeSaved);
        var savedEntity = textAnalysisRepository.save(entityJpa);
        return textAnalysisMapper.toTextAnalysis(savedEntity);
    }

    @Override
    public TextAnalysis update(Long textAnalysisId, String textAnalysisUrl, Integer jobState) {
        var jpaEntityOptional = textAnalysisRepository.findByTranscriptionId(textAnalysisId);
        /*if(jpaEntityOptional.isPresent()){
            var jpaEntity = jpaEntityOptional.get();
            jpaEntity.setText_analysis_url(textAnalysisUrl);
            jpaEntity.setJob_state(jobState);
            var entityUpdated = textAnalysisRepository.save(jpaEntity);
            var textAnalysisUpdated = textAnalysisMapper.toTextAnalysis(entityUpdated);
            return  textAnalysisUpdated;
        }*/
        jpaEntityOptional.setText_analysis_url(textAnalysisUrl);
        jpaEntityOptional.setJob_state(jobState);
        var entityUpdated = textAnalysisRepository.save(jpaEntityOptional);
        var textAnalysisUpdated = textAnalysisMapper.toTextAnalysis(entityUpdated);
        return  textAnalysisUpdated;
    }

    @Override
    public TextAnalysis get(Long transcriptionId) {
        var textAnalysis = textAnalysisRepository.findByTranscriptionId(transcriptionId);
        return textAnalysisMapper.toTextAnalysis(textAnalysis);
    }
}
