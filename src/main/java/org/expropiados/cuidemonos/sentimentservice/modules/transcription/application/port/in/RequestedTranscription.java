package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in;

import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Value
@EqualsAndHashCode(callSuper = false)
public class RequestedTranscription {
    @NotNull
    String videocallDir;

    @NotNull
    UUID videocallUuid;

    @NotNull
    String userId;

    @NotNull
    Long appointmentId;

    public RequestedTranscription(String videocallDir, UUID videocallUuid, String userId, Long appointmentId) {
        this.videocallDir = videocallDir;
        this.videocallUuid = videocallUuid;
        this.userId = userId;
        this.appointmentId = appointmentId;
    }
}
