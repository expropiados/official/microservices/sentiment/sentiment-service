package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.out.persistence.textAnalysis;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface SpringJpaTextAnalysisRepository extends JpaRepository<TextAnalysisJpaEntity, Long> {
    @Query(value = "SELECT T.text_analysis_id,T.job_state,T.text_analysis_url,T.transcription_id " +
            "FROM sentiment.text_analysis T " +
            "WHERE T.transcription_id=?1", nativeQuery = true)
    TextAnalysisJpaEntity findByTranscriptionId(Long id);
}
