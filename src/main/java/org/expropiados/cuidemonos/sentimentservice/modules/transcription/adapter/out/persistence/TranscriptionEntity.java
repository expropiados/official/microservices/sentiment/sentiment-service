package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.out.persistence;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
@Table(name = "transcription")
public class TranscriptionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transcription_id")
    private Long transcriptionId;

    @Column(name = "videocall_uuid")
    private UUID videocallUuid;

    @Column(name = "appointment_id")
    private Long appointmentId;

    @Column(name = "user_id", length = 100)
    private String userId;

    @Column(name = "transcription_url", length = 100)
    private String transcriptionUrl;

    @Column(name = "job_state")
    private Integer jobState;

    @Column(name = "request_datetime")
    private Date requestDatetime;

    @Column(name = "termination_datetime")
    private Date terminationDatetime;

    @Column(name = "audio_url", length = 100)
    private String audioUrl;
}
