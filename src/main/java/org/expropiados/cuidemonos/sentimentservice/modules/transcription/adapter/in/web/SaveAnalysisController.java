package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.in.web;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.SaveAnalysisUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.UpdateAnalysisUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
public class SaveAnalysisController {
    private final UpdateAnalysisUseCase updateAnalysisUseCase;

    @PostMapping("/saveAnalysis")
    @ApiOperation(value="Saves a text analysis of a transcription")
    public TextAnalysis saveAnalysis(@RequestParam(value="text_analysis_id") Long textAnalysisId,
                                     @RequestParam(value="analysis_url") String textAnalysisUrl,
                                     @RequestParam(value="job_state") Integer jobState){
        return updateAnalysisUseCase.updateAnalysis(textAnalysisId, textAnalysisUrl, jobState);
    }
}
