package org.expropiados.cuidemonos.sentimentservice.modules.text_analysis.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;

@Data
@Builder
@AllArgsConstructor
public class TextAnalysis {
    private Long id;
    private ArrayList<String> word;
    private ArrayList<Long> position;
}
