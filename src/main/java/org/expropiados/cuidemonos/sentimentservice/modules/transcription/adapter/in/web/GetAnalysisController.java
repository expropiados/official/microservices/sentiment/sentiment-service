package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.in.web;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.expropiados.cuidemonos.sentimentservice.hexagonal.WebAdapter;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.GetAnalysisUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.SaveTranscriptionUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.UUID;

@WebAdapter
@RestController
@CrossOrigin
@RequiredArgsConstructor
public class GetAnalysisController {
    private final GetAnalysisUseCase getAnalysisUseCase;

    @PostMapping("/getAnalysis")
    @ApiOperation(value="Returns a transcription state")
    public TextAnalysis getTextAnalysis(@RequestParam(value="transcription_id") Long transcription_id,
                                        @RequestParam(value="vdeocall_uuid") String vdeocall_uuid
                                        ){
        UUID vdeocall_uuid_uuid = UUID.fromString((vdeocall_uuid));
        return getAnalysisUseCase.getTextAnalysis(transcription_id
                );
    }
}
