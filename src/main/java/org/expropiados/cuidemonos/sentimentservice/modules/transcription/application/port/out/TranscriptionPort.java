package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;

import java.util.UUID;

public interface TranscriptionPort {
    Long save(Transcription transcription);
    Transcription getTranscription(Long appointmentId);

    Transcription saveTranscription(Transcription transcription);
}
