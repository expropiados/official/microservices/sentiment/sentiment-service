package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;

import java.util.UUID;

public interface GetTranscriptionUseCase {
    Transcription getTranscription(Long appointmentId);
}
