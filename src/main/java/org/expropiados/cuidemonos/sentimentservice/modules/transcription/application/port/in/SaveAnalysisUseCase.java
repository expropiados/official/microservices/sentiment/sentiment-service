package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;

public interface SaveAnalysisUseCase {
    TextAnalysis saveAnalysis(Long transcriptionId);
}
