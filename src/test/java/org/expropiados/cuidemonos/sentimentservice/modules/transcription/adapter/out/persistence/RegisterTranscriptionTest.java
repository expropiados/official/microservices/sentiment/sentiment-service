package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.out.persistence;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@Import({TranscriptionPersistenceAdapter.class})
@ComponentScan(basePackageClasses = {TranscriptionMapper.class})
@ContextConfiguration
class RegisterTranscriptionTest {
    @Autowired
    private TranscriptionPersistenceAdapter transcriptionPersistenceAdapter;

    @Test
    void registerTranscription(){
        var uuid = UUID.randomUUID();
        var audioUrl = "www.test.com/audio.mp3";

        var transcription = new Transcription();
        transcription.setJobState(0);
        transcription.setVideocallUuid(uuid);
        transcription.setAudioUrl(audioUrl);

        var transcriptionId = transcriptionPersistenceAdapter.save(transcription);

        assertThat(transcriptionId)
                .isNotNull()
                .isInstanceOf(Long.class);
    }
}
