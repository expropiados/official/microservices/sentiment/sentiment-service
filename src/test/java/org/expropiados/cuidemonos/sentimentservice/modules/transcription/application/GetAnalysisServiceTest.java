package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TextAnalysisPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetAnalysisServiceTest {
    private final TextAnalysisPort textAnalysisPort = Mockito.mock(TextAnalysisPort.class);
    private final GetAnalysisService getAnalysisService = new GetAnalysisService(textAnalysisPort);

    @Test
    void getAnalysisSuccess(){
        var textAnalysis = new TextAnalysis(1L, 1L, "http://www.google.com", 1);
        when(textAnalysisPort.get(1L)).thenReturn(textAnalysis);
        var result = getAnalysisService.getTextAnalysis(1L);
        assertThat(result.getId()).isEqualTo(1L);
    }

    @Test
    void getAnalysisEmpty(){
        when(textAnalysisPort.get(1L)).thenReturn(null);
        var result = getAnalysisService.getTextAnalysis(1L);
        assertThat(result).isNull();
    }
}
