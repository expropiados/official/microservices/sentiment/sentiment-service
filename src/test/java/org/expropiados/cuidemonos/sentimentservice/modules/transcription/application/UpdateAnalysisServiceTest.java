package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TextAnalysisPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class UpdateAnalysisServiceTest {
    private final TextAnalysisPort textAnalysisPort = Mockito.mock(TextAnalysisPort.class);
    private final UpdateAnalysisService updateAnalysisService = new UpdateAnalysisService(textAnalysisPort);

    @Test
    void updateAnalysisSuccess(){
        var textAnalysis = new TextAnalysis(1L, 1L, "http://www.google.com", 1);
        when(textAnalysisPort.update(1L, "https://www.google.com", 1)).thenReturn(textAnalysis);
        var result = updateAnalysisService.updateAnalysis(1L, "https://www.google.com", 1);
        assertThat(result.getId()).isEqualTo(textAnalysis.getId());
        assertThat(result.getTranscriptionId()).isEqualTo(textAnalysis.getTranscriptionId());
    }

    @Test
    void updateAnalysisEmpty(){
        when(textAnalysisPort.get(1L)).thenReturn(null);
        var result = updateAnalysisService.updateAnalysis(1L, "https://www.google.com", 1);
        assertThat(result).isNull();
    }
}
