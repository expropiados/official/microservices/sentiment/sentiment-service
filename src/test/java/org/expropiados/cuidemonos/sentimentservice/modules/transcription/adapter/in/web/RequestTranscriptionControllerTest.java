package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.in.web;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.RequestTranscriptionUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = RequestTranscriptionController.class)
class RequestTranscriptionControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private RequestTranscriptionUseCase requestTranscriptionUseCase;

    @Test
    void requestVideocallSuccess() throws Exception {
        UUID videocallUuid = UUID.randomUUID();
        Long appointmentId = 5L;
        String audioUrl = "www.test.com/audio.mp3";
        String userId = "2";
        String videocallDir = "mainvideocall";
        String params = String.format("{" +
                "\"audioUrl\":\"%s\"," +
                "\"videocallDir\":\"%s\"," +
                "\"videocallUuid\":\"%s\"," +
                "\"userId\":\"%s\"," +
                "\"appointmentId\":\"%s\"" +
                "}", audioUrl, videocallDir, videocallUuid, userId, appointmentId);

        mockMvc.perform(post("/transcribe").
                content(params).
                header("Content-Type", "application/json")).andExpect(status().isOk());

        then(requestTranscriptionUseCase).should().requestTranscription(videocallDir, videocallUuid, userId, appointmentId);
    }
}