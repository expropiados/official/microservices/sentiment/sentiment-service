package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TranscriptionPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.Transcription;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class GetTranscriptionServiceTest {

    private final TranscriptionPort transcriptionPort = Mockito.mock(TranscriptionPort.class);
    private final GetTranscriptionService getTranscriptionService = new GetTranscriptionService(transcriptionPort);

    @Test
    void getTranscriptionSuccess(){
        var transcription = Transcription.builder().appointmentId(1L).build();
        when(transcriptionPort.getTranscription(1L)).thenReturn(transcription);
        var result = getTranscriptionService.getTranscription(1L);
        assertThat(result.getAppointmentId()).isEqualTo(transcription.getAppointmentId());
    }

    @Test
    void getTranscriptionEmpty(){
        when(transcriptionPort.getTranscription(1L)).thenReturn(null);
        var result = getTranscriptionService.getTranscription(1L);
        assertThat(result).isNull();
    }
}
