package org.expropiados.cuidemonos.sentimentservice.modules.transcription.adapter.in.web;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.SaveAnalysisUseCase;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.in.UpdateAnalysisUseCase;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = SaveAnalysisController.class)
class SaveAnalysisControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UpdateAnalysisUseCase updateAnalysisUseCase;

    @Test
    void saveAnalysisSuccess() throws Exception {
        Long textAnalysisId = 20L;
        String textAnalysisUrl = "www.test.com";
        Integer jobState = 1;

        mockMvc.perform(post("/saveAnalysis")
                .param("text_analysis_id", String.valueOf(textAnalysisId))
                .param("analysis_url", textAnalysisUrl)
                .param("job_state", String.valueOf(jobState))
                .header("Content-Type", "application/json"))
                .andExpect(status().isOk());

        then(updateAnalysisUseCase).should().updateAnalysis(textAnalysisId, textAnalysisUrl, jobState);
    }
}