package org.expropiados.cuidemonos.sentimentservice.modules.transcription.application;

import org.expropiados.cuidemonos.sentimentservice.modules.transcription.application.port.out.TextAnalysisPort;
import org.expropiados.cuidemonos.sentimentservice.modules.transcription.domain.TextAnalysis;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
class SaveAnalysisServiceTest {
    private final TextAnalysisPort textAnalysisPort = Mockito.mock(TextAnalysisPort.class);
    private final SaveAnalysisService saveAnalysisService = new SaveAnalysisService(textAnalysisPort);

    @Test
    void saveAnalysisSuccess(){
        var analysis = TextAnalysis.builder().id(1L).transcriptionId(1L).build();
        when(textAnalysisPort.save(1L)).thenReturn(analysis);
        var result = saveAnalysisService.saveAnalysis(1L);

        assertThat(result.getId()).isEqualTo(analysis.getId());
    }

    @Test
    void saveAnalysisEmpty(){
        when(textAnalysisPort.get(1L)).thenReturn(null);
        var result = saveAnalysisService.saveAnalysis(1L);

        assertThat(result).isNull();
    }
}
